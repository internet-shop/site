<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Product;
use App\Category;

class ProductController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Product';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Product());

        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'));
        $grid->column('category.name', __('Category id'));
        $grid->column('code', __('Code'));
        $grid->column('price', __('Price'));
        $grid->column('availability', __('Availability'));
        $grid->column('brand', __('Brand'));
        $grid->column('description', __('Description'));
        $grid->column('is_new', __('Is new'));
        $grid->column('is_recommended', __('Is recommended'));
        $grid->column('status', __('Status'));
        $grid->column('image', __('Image'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Product::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('category_id', __('Category id'));
        $show->field('code', __('Code'));
        $show->field('price', __('Price'));
        $show->field('availability', __('Availability'));
        $show->field('brand', __('Brand'));
        $show->field('description', __('Description'));
        $show->field('is_new', __('Is new'));
        $show->field('is_recommended', __('Is recommended'));
        $show->field('status', __('Status'));
        $show->field('image', __('Image'))->image();

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Product());

        $form->text('name', __('Name'));
        $form->select('category_id', __('Category id'))->options(Category::all()->pluck('name', 'id'))
        ->rules('required');
        $form->number('code', __('Code'));
        $form->decimal('price', __('Price'));
        $form->radio('availability', __('Availability'))->options(['1' => 'Есть в наличии', '0' => 'Отсуствует']);
        $form->text('brand', __('Brand'));
        $form->textarea('description', __('Description'));
        $form->radio('is_new', __('Is new'))->options(['1' => 'Да', '0' => 'Нет']);
        $form->radio('is_recommended', __('Is recommended'))->options(['1' => 'Да', '0' => 'Нет']);
        $form->radio('status', __('Status'))->options(['1' => 'Включить', '0' => 'Отключить']);
        $form->image('image', __('Image'))
            ->rules('required')
            ->uniqueName()
            ->removable()
            ->autofocus();
        return $form;
    }
}
