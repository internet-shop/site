<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class CartController extends Controller
{
    
    public function cart()
    {
        $orderId = session('orderId');
        // dd($orderId);
        if (!is_null($orderId)){
            $order = Order::findOrFail($orderId);
            // var_dump($order);
            return view('cart', compact('order'));
        } else {
            return redirect()->route('shop');
        };
    }
    //подверждение заказа
    public function cartConfirm(Request $request)
    {
        $orderId = session('orderId');
        if (is_null($orderId)){
            return redirect()->route('shop');
        }
        $order = Order::find($orderId);
        $order->name = $request->name;
        $order->phone = $request->phone;
        $order->surname = $request->surname;
        $order->message = $request->message;
        $order->delivery_address = $request->delivery_address;
        $order->status = 1;
        
        $order->save();
        
        return redirect()->route('shop');
    }
    //просмтор заказа
    public function cartPlace()
    {
        $orderId = session('orderId');
        if (is_null($orderId)){
            return redirect()->route('shop');
        }
        if (!is_null($orderId)){
            $order = Order::findOrFail($orderId);
            return view('cartplace', compact('order'));
        } else {
            return redirect()->route('index');
        };
    }
    // Добавляем товары в корзину
    public function cartAdd($productId)
    {
        $orderId = session('orderId');
        if (is_null($orderId)) {
            $orderId = Order::create()->id;
            session(['orderId' => $orderId]);
            $order = $orderId;
        }
        else {
            $orderId = Order::find($orderId);
            $order = $orderId;
        }
        // dump($order);
        
        if ($order->products->contains($productId)) {
            $pivotRow = $order->products()->where('product_id', $productId)->first()->pivot;
            $pivotRow->count++;
            $pivotRow->update();
        } else {
            $order->products()->attach($productId);
        }
        
        return redirect()->route('cart');
    }

    public function cartRemove($productId)
    {
        $orderId = session('orderId');
        if (is_null($orderId)) {
            return redirect()->route('cart');;
        }
       
        $order = Order::find($orderId);

        if ($order->products->contains($productId)) {
            $pivotRow = $order->products()->where('product_id', $productId)->first()->pivot;
            if ($pivotRow->count < 2) {
                $order->products()->detach($productId);
            } else {
                $pivotRow->count--;
                $pivotRow->update();
                // dd($pivotRow);
            }   
        }
        // dump($order->products);
        return redirect()->route('cart');
    }

}
