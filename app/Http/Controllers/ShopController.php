<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;

class ShopController extends Controller
{
    public function index()
    {
        $products = Product::paginate(4);
        $category = Category::getCategory();
        return view('shop', compact('products'), compact('category'));
    }

    public function oneCategory($id)
    {
        $products = Product::where('category_id', $id)->paginate(6);
        $category = Category::getCategory();
        return view('shop', ['products' => $products], compact('category'));
    }

    public function oneProduct($id)
    {
        $products = Product::find($id);
        $category = Category::getCategory();
        return view('productdetails', compact('products'), compact('category'));
    }


}
