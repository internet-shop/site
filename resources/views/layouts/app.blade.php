<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>@yield('title-block')</title>
        <link href="css/app.css" rel="stylesheet" type="text/css">
        <link href="/css/all.css" rel="stylesheet" type="text/css">
        <script src="{{ URL::asset('js/app.js') }}"></script>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    </head><!--/head-->

    <body>
        
        @if (Route::current()->getName() == 'users')
        @include('inc.headerusers')
        @else
        @include('inc.header')
        @endif

        @yield('content')

        @if (Route::current()->getName() == 'users')
        @include('inc.footerusers')
        @else
        @include('inc.footer')
        @endif

    </body>
</html>