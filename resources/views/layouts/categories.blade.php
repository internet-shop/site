<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <div class="left-sidebar">
                <h2>Каталог</h2>
                <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                    {{-- {{dd($products->getCategory())}} --}}
                    {{-- {{dd($products)}} --}}
                    @foreach($category as $el)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <h4 class="panel-title"><a href="/category/{{$el['id']}}">{{ $el['name'] }}</a></h4>
                        </div>
                    </div>
                    @endforeach
                </div><!--/category-products-->