@extends('layouts.app')


@section('title-block')
    E-shopper
@endsection

@section('content')
<section>
@include('layouts.categories')


                        </div>
                    </div>

                    <div class="col-sm-9 padding-right">
                        <div class="product-details"><!--product-details-->
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="view-product">
                                    <img src="/storage/{{ $products['image'] }}" alt="" />
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="product-information"><!--/product-information-->
                                        <img src="images/product-details/new.jpg" class="newarrival" alt="" />
                                        <h2>{{ $products['name'] }}</h2>
                                        <p>Код товара: {{ $products['code'] }}</p>
                                        <span>
                                            <span>US ${{ $products['price'] }}</span>
                                            <label>Количество:</label>
                                            <input type="text" value="1" />
                                            <button type="button" class="btn btn-fefault cart">
                                                <i class="fa fa-shopping-cart"></i>
                                                В корзину
                                            </button>
                                        </span>
                                        {{-- <p><b>Наличие:</b> На складе</p> --}}
                                        <p><b>Состояние:</b> Новое</p>
                                        <p><b>Производитель:</b> {{ $products['brand'] }}</p>
                                    </div><!--/product-information-->
                                </div>
                            </div>
                            <div class="row">                                
                                <div class="col-sm-12">
                                    <h5>Описание товара</h5>
                                    <p>{{ $products['description'] }}</p>
                                </div>
                            </div>
                        </div><!--/product-details-->

                    </div>
                </div>
            </div>


@endsection