@extends('layouts.app')


@section('title-block')
    E-shopper
@endsection

@section('content')
<section>
@include('layouts.categories')


                <div class="shipping text-center"><!--shipping-->
                    <img src="images/home/shipping.jpg" alt="" />
                </div><!--/shipping-->

            </div>
        </div>

        <div class="col-sm-9 padding-right">
            <div class="features_items"><!--features_items-->
            <h2 class="title text-center">Товары категории</h2>
                @foreach ($products as $item)
                    
                <div class="col-sm-4">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                            <img src="{{ $item['image'] }}" alt="" />
                            <h2>{{ $item['price'] }}$</h2>
                            <a href="product/{{ $item['id'] }}"><p>{{ $item['name'] }}</p></a>
                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>В корзину</a>
                            </div>
                            {{-- <div class="product-overlay">
                                <div class="overlay-content">
                                    <h2>$56</h2>
                                    <p>Easy Polo Black Edition</p>
                                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>В корзину</a>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
                @endforeach
            </div><!--features_items-->
            {{ $products->links() }}
        </div>
    </div>
</div>
</section>
@endsection