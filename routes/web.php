<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('index');

Route::get('/login', ['as' => 'login', function () {
    // dd(Route::has('users'));
    return view('auth.login');
}]);

Route::get('/logout', 'Auth\LoginController@logout')->name('get-logout');

Route::get('/shop', 'ShopController@index')->name('shop');
Route::get('/InitPayment', 'InitPaymentController@index')->name('InitPayment');
Route::get('/category/{id}', 'ShopController@oneCategory');
Route::get('/product/{id}', 'ShopController@oneProduct');

Route::get('/contact', function () {
    return view('contact');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/cart', 'CartController@cart')->name('cart');
Route::get('/cart/place', 'CartController@cartPlace')->name('cart-place');
Route::post('/cart/place', 'CartController@cartConfirm')->name('cart-confirm');

Route::post('/cart/add/{id}', 'CartController@cartAdd')->name('cart-add');
Route::post('/cart/remove/{id}', 'CartController@cartRemove')->name('cart-remove');
